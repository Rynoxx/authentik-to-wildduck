const axios = require("axios"),
    dotenv = require("dotenv");

dotenv.config();

const requiredAuthentikGroups = (process.env.AUTHENTIK_REQUIRED_GROUPS ? process.env.AUTHENTIK_REQUIRED_GROUPS : "").split(",");

const wildduckUsersURL = new URL(process.env.WILDDUCK_BASE_URL + "/users");
const authentikUsersURL = new URL(process.env.AUTHENTIK_BASE_URL + "/api/v2beta/core/users/");

const wildduckToken = "";
const authentikToken = "7b9545401a254fe0857f20fa35f0629d";

const authentikHeaders = {
    "Authorization": "Bearer " + authentikToken,
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json"
};

const wildduckHeaders = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json"
};

// Authorization token isn't mandatory for the wildduck setup, only add token if configured.
if (typeof process.env.WILDDUCK_AUTH_TOKEN === "string" && process.env.WILDDUCK_AUTH_TOKEN.length > 0) {
    wildduckHeaders["X-Access-Token"] = wildduckToken;
}

let wildduckUsers = [];
let usersToCreate = [];
let usersToDelete = [];

function getWildduckPaginated(url, params, results) {
    if (typeof url === "string") {
        url = new URL(url);
    }

    results = typeof results === "undefined" ? [] : results;

    return new Promise((resolve, reject) => {
        axios.get(url.toString(), params).then(response => {
            if (!!response.data.results && response.data.results.length > 0) {
                results = results.concat(response.data.results);
            }

            if (response.data.nextCursor) {
                url.searchParams.set("next", response.data.nextCursor);
                return getWildduckPaginated(url, params, results);
            } else {
                return results;
            }
        }).then(res => {
            resolve(res);
        }).catch(reject);
    });
}

function getAuthentikPaginated(url, params, results) {
    if (typeof url === "string") {
        url = new URL(url);
    }

    results = typeof results === "undefined" ? [] : results;

    return new Promise((resolve, reject) => {
        axios.get(url.toString(), params).then(response => {
            if (!!response.data.results && response.data.results.length > 0) {
                results = results.concat(response.data.results);
            }

            if (response.data.pagination.next && response.data.pagination.next > 0) {
                url.searchParams.set("page", response.data.pagination.next);
                return getAuthentikPaginated(url, params, results);
            } else {
                return results;
            }
        }).then(res => {
            resolve(res);
        }).catch(reject);
    });
}

getWildduckPaginated(wildduckUsersURL, { headers: wildduckHeaders }).then(users => {
    wildduckUsers = users;

    return getAuthentikPaginated(authentikUsersURL, { "headers": authentikHeaders });
}).then(authentikUsers => {
    console.debug(authentikUsers);
    console.debug(wildduckUsers);

    for (let i = 0; i < authentikUsers.length; i++) {
        let user = authentikUsers[i];

        if (requiredAuthentikGroups.length > 0) {
            let hasGroup = false;

            for (let g = 0; g < user.groups.length; g++) {
                if (requiredAuthentikGroups.indexOf(user.groups[g].name) > -1) {
                    hasGroup = true;
                    break;
                }
            }

            if (!hasGroup) {
                continue;
            }
        }

        let userExists = false;

        for (let j = 0; j < wildduckUsers.length; j++) {
            if (user.username === wildduckUsers[j].username) {
                userExists = true;
                break;
            }
        }

        if (!userExists) {
            usersToCreate.push(authentikUsers[i]);
        }
    }

    for (let i = 0; i < wildduckUsers.length; i++) {
        let userExists = false;

        for (let j = 0; j < authentikUsers.length; j++) {
            if (requiredAuthentikGroups && requiredAuthentikGroups.length > 0) {
                let hasGroup = false;

                for (let g = 0; g < authentikUsers[j].groups.length; g++) {
                    if (requiredAuthentikGroups.indexOf(authentikUsers[j].groups[g].name) > -1) {
                        hasGroup = true;
                        break;
                    }
                }

                if (!hasGroup) {
                    continue;
                }
            }

            if (authentikUsers[j].username === wildduckUsers[i].username) {
                userExists = true;
                break;
            }
        }

        if (!userExists) {
            usersToDelete.push(wildduckUsers[i]);
        }
    }

    console.debug(usersToCreate);
    console.debug(usersToDelete);

    if (usersToCreate.length > 0) {
        console.log("Creating " + usersToCreate.length + " users in wildduck.");

        let usersToCreateRequests = [];

        for (let i = 0; i < usersToCreate.length; i++) {
            let user = usersToCreate[i];

            let payload = {
                "username": user.username,
                "name": user.name,
                "password": false,
                "address": user.email
            };

            usersToCreateRequests.push(axios.post(wildduckUsersURL.toString(), JSON.stringify(payload), { headers: wildduckHeaders }));
        }

        return Promise.all(usersToCreateRequests);
    } else {
        console.log("No users to create in wildduck.");
        return [];
    }
}).then(creationResult => {
    console.debug(creationResult);

    if (usersToDelete.length > 0) {
        console.log("Deleting " + usersToDelete.length + " users from wildduck.");

        let usersToDeleteRequests = [];
        for (let i = 0; i < usersToDelete.length; i++) {
            if (!usersToDelete.hasOwnProperty(i)) {
                continue;
            }

            usersToDeleteRequests.push(axios.delete(wildduckUsersURL.toString() + "/" + usersToDelete[i].id));
        }

        return Promise.all(usersToDeleteRequests);
    } else {
        console.log("No users to delete from wildduck.");
        return [];
    }
}).then(deletionResult => {
    console.debug(deletionResult);
});
